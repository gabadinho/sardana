.. currentmodule:: sardana.macroserver.scan

:mod:`~sardana.macroserver.scan.recorder`
=========================================

.. automodule:: sardana.macroserver.scan.recorder

.. rubric:: Classes

.. hlist::
    :columns: 3

    * :class:`BaseFileRecorder`
    * :class:`DataHandler`
    * :class:`DataRecorder`

DataHandler
-----------

.. inheritance-diagram:: DataHandler
    :parts: 1

.. autoclass:: DataHandler
    :show-inheritance:
    :members:

DataRecorder
------------

.. inheritance-diagram:: DataRecorder
    :parts: 1

.. autoclass:: DataRecorder
    :show-inheritance:
    :members:

BaseFileRecorder
----------------

.. inheritance-diagram:: BaseFileRecorder
    :parts: 1

.. autoclass:: BaseFileRecorder
    :show-inheritance:
    :members:
